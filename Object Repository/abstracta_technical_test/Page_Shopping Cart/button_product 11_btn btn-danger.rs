<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_product 11_btn btn-danger</name>
   <tag></tag>
   <elementGuidId>3920b853-a323-4a58-8a0a-c181e31422f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[9]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.input-group-btn > button.btn.btn-danger</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>345ec812-4e49-4132-bf00-ec00b4f2d80e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4d8e6307-1b41-4c83-ac34-8dae94e3ebf1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>tooltip</value>
      <webElementGuid>2c098fe3-447b-494c-b617-1b5a60d7f6a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-danger</value>
      <webElementGuid>00dcae65-c27e-4f56-a1d7-2e860dddd5a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>cart.remove('263766');</value>
      <webElementGuid>4c7905f3-77a8-444f-ba77-a8e3983c0953</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-original-title</name>
      <type>Main</type>
      <value>Remove</value>
      <webElementGuid>176f4db5-6673-405a-87bc-e23d9d0ef7d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>tooltip910317</value>
      <webElementGuid>7a22b0ef-dd86-43a6-b586-6fbef0188ef9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/form[1]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-bordered&quot;]/tbody[1]/tr[1]/td[@class=&quot;text-left&quot;]/div[@class=&quot;input-group btn-block&quot;]/span[@class=&quot;input-group-btn&quot;]/button[@class=&quot;btn btn-danger&quot;]</value>
      <webElementGuid>80b68938-f2e9-4287-be1c-0706b86ab73c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[9]</value>
      <webElementGuid>cfd9e34c-0100-4c95-87a4-29b3c3ee3501</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/form/div/table/tbody/tr/td[4]/div/span/button[2]</value>
      <webElementGuid>fcff9b38-e1b5-4676-8ea5-62b7cd6ed66a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='product 11'])[1]/following::button[2]</value>
      <webElementGuid>9cbc0d27-5789-408c-97d0-1d218a1d938b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='***'])[1]/following::button[2]</value>
      <webElementGuid>76a8ecbb-d5ee-4a6e-a034-e8b19ea2fc61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$123.20'])[3]/preceding::button[1]</value>
      <webElementGuid>861f5d88-00c4-447f-8ece-9854049559e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$123.20'])[4]/preceding::button[1]</value>
      <webElementGuid>90ae97f8-0fca-4a9f-b1bc-25e157ba0a2f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]</value>
      <webElementGuid>84259dc2-b506-4f6e-b57f-917c091ebe17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button']</value>
      <webElementGuid>44f942ae-aa3d-4b76-8e0f-a429bb9cdc5b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
