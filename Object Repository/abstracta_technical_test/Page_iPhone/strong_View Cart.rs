<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>strong_View Cart</name>
   <tag></tag>
   <elementGuidId>3e10c087-6ca5-4c1c-8ba7-8c93e232f116</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cart']/ul/li[2]/div/p/a/strong</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a > strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>6eebed54-9fd7-48cd-95d4-9011f25c35b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> View Cart</value>
      <webElementGuid>3d01497b-8146-4bd2-b371-bd83cd02e384</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cart&quot;)/ul[@class=&quot;dropdown-menu pull-right&quot;]/li[2]/div[1]/p[@class=&quot;text-right&quot;]/a[1]/strong[1]</value>
      <webElementGuid>dc17075a-791a-4592-ba89-3ebf8a628315</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='cart']/ul/li[2]/div/p/a/strong</value>
      <webElementGuid>581e3c75-dac1-4dff-bfed-e592218b1fcf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$123.20'])[2]/following::strong[1]</value>
      <webElementGuid>93d376bf-40c1-4d76-98be-25465a25650b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total'])[1]/following::strong[1]</value>
      <webElementGuid>ee606858-5dbe-4801-ba5d-fe7c7f088bc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[2]/preceding::strong[1]</value>
      <webElementGuid>83dbe0ad-67c2-4f0b-bb6c-4bd7da560d8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Categories'])[1]/preceding::strong[2]</value>
      <webElementGuid>b2c19c76-ef85-41d3-9a13-6886d045c28d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='View Cart']/parent::*</value>
      <webElementGuid>ded270d1-0166-402d-a252-5fb87ad62781</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/strong</value>
      <webElementGuid>308d8f04-3c6c-48c3-a476-6c255a26f8a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = ' View Cart' or . = ' View Cart')]</value>
      <webElementGuid>851de883-f2cb-43a8-a340-202898f19192</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
