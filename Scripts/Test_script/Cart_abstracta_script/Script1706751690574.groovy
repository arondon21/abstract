import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')
WebUI.navigateToUrl('https://opencart.abstracta.us/')
WebUI.maximizeWindow()
WebUI.setText(findTestObject('Object Repository/buscador'), 'Iphone') 
WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/abstracta_technical_test/Page_Your Store/button_Your Store_btn btn-default btn-lg'))
WebUI.click(findTestObject('Object Repository/abstracta_technical_test/Page_Search - iphone/img_Show_img-responsive'))
WebUI.click(findTestObject('Object Repository/add to cart'))
WebUI.click(findTestObject('Object Repository/abstracta_technical_test/Page_iPhone/button_1 item(s) - 123.20'))
WebUI.delay(2)
WebUI.click(findTestObject('Object Repository/view cart'))
WebUI.verifyTextPresent("product 11", true)
WebUI.click(findTestObject('Object Repository/abstracta_technical_test/Page_Shopping Cart/button_product 11_btn btn-danger'))
WebUI.verifyTextPresent("Your shopping cart is empty!", true)
WebUI.click(findTestObject('Object Repository/abstracta_technical_test/Page_iPhone/button_1 item(s) - 123.20'))
WebUI.verifyTextPresent("Your shopping cart is empty!", true)